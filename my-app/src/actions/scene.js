export const addSphere = (xCoord, zCoord) => {
    return {
        type: "ADD_SPHERE",
        xCoord,
        zCoord
    }
}

export const changeXCoord = (xCoord) => {
    return {
        type: "CHANGE_XCOORD",
        xCoord
    }
}

export const changeZCoord = (zCoord) => {
    return {
        type: "CHANGE_ZCOORD",
        zCoord
    }
}

export const deleteSphere = (sphereId) => {
    return {
        type: "DELETE_SPHERE",
        sphereId
    }
}
