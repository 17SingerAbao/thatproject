import thunk from 'redux-thunk'
import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import sceneReducer from './reducers/scene'

const saveState = (state) => {
	try {
		const serializedState = JSON.stringify({...state, router: undefined})
		localStorage.setItem('state', serializedState)
	} catch (err) {
		// eslint-disable-next-line no-console
		console.error(err)
	}
}

const loadState = () => {
	try {
		const serializedState = localStorage.getItem('state')
		if (serializedState === null) {
			return undefined
		}
		const state = JSON.parse(serializedState)
		return state
	} catch (err) {
		// eslint-disable-next-line no-console
		console.error(err)
		return undefined
	}
}

export const history = createBrowserHistory()

const createRootReducer = (rootHistory) => combineReducers({
  router: connectRouter(rootHistory),
  scene: sceneReducer

})

const configureStore = (preloadedState) => {
	const composeEnhancers = (window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
	const newStore = createStore(
		createRootReducer(history),
		preloadedState,
		composeEnhancers(
			applyMiddleware(
				thunk,
				routerMiddleware(history),
			),
		),
	)

	newStore.subscribe(() => {
		saveState(newStore.getState())
	})
	return newStore
}

export const store = configureStore(loadState())