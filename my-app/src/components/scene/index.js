import 'aframe'
import { Entity } from 'aframe-react';
import React from 'react';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux'

import {addSphere, deleteSphere, changeXCoord, changeZCoord} from '../../actions/scene'


const SceneComponent = (props) => {
	let scaleFactor = 1
	let floorWidth = 500 * scaleFactor
	let floorHeight = 500 * scaleFactor
	let wallDepth = 1 * scaleFactor
	let wallHeight = 10 * scaleFactor
	let sphereRadius = 5 * scaleFactor

	const onClearStore = () => {
		localStorage.setItem('state', '{}')
		alert('Game state cleared. Please refresh the page.')
	}

	return (
		<div>
			<div style={{float: "right"}}>
				<TextField
					id="standard-name"
					label="X Coordinate [-250: 250]"
					onChange={(e) => {
						props.onXCoordChanged(e.target.value)

					}}
					margin="normal"
				/>
				<br/>
				<TextField
					id="standard-name"
					label="Z Coordinate [-250: 250]"
					onChange={(e) => {
						props.onZCoordChanged(e.target.value)
					}}
					margin="normal"
				/> 
				<br/>


				<Button 
					variant="contained"
					onClick={() => {
						props.onSphereAdded(props.xCoord, props.zCoord, floorWidth)
					}}
				> 
					Create Sphere
				</Button>

				<Button 
					variant="contained"
					onClick={onClearStore}
				> 
					Clear Scene
				</Button>

				<Typography>
					Sphere Array:
				</Typography>
				{
					props.sphereList.map((coord, index) => {
						return(
							<div key={index}>
								<Button 
									disabled
									variant="contained" 
									color="secondary"
								>
									X: {coord.xCoord}, Z: {coord.zCoord}
								</Button>

								<Button 
									variant="contained" 
									color="primary"

									onClick={() => {
										props.onSphereDeleted(index)
									}}
								>
									Delete
								</Button>
								<br/>
							</div>
						)
					})
				}
			</div>
			
			<a-scene embedded style={{height: "900px", width: "900px"}}>

				<a-entity camera="active: true" look-controls  wasd-controls>
					<a-camera camera="active: true" position="0 300 300">
					</a-camera>
				</a-entity>

				
				<a-sky color="#222"></a-sky>
				{/* walls and floor */}
				<Entity geometry={{ primitive: 'plane', width: floorWidth, height: floorHeight }} material={{ color: 'grey', side: "double"}} position={{ x: 0, y: 0, z: 0 }} rotation={{x: -90, y: 0, z: 0}} />
				<Entity geometry={{ primitive: 'box', width: floorWidth, height: wallHeight, depth: wallDepth}} material={{ color: '#85C1E9', side: "double"}} position={{ x: 0, y: wallHeight/2 , z: -floorWidth/2  }} rotation={{x: 0, y: 0, z: 0}} />
				<Entity geometry={{ primitive: 'box', width: floorWidth, height: wallHeight, depth: wallDepth}} material={{ color: '#85C1E9', side: "double"}} position={{ x: floorWidth/2 , y: wallHeight/2 , z: 0 }} rotation={{x: 0, y: 90, z: 0}} />
				<Entity geometry={{ primitive: 'box', width: floorWidth, height: wallHeight, depth: wallDepth}} material={{ color: '#85C1E9', side: "double"}} position={{ x: -floorWidth/2 , y: wallHeight/2 , z: 0 }} rotation={{x: 0, y: 90, z: 0}} />
				<Entity geometry={{ primitive: 'box', width: floorWidth, height: wallHeight, depth: wallDepth}} material={{ color: '#85C1E9', side: "double"}} position={{ x: 0, y: wallHeight/2 , z: floorWidth/2 }} rotation={{x: 0, y: 0, z: 0}} />
				{/* coordinaters */}
				<a-entity 
					line="start: 0, 1, 0; end: 0 250 0; color: red"
					line__2="start: 0, 1, 0; end: 0 1 250; color: green"
					line__3="start: 0, 1, 0; end: 250 1 0; color: blue">
			
				</a-entity>
		
				<a-entity id="mouseCursor" cursor="rayOrigin: mouse" raycaster="objects: #sphere"></a-entity>

				{
					props.sphereList.map((coord, index) => {
						return(
							<Entity 
								id="sphere"
								key={index} 
								geometry={{ primitive: 'sphere', radius: sphereRadius}} 
								material={{ color: 'pink', side: "double"}} 
								position={{ x: coord.xCoord, y: sphereRadius, z: coord.zCoord}}
								animation__mouseenter={{property: 'components.material.material.color', type: 'color', from: 'pink', to: 'brown', startEvents: 'mouseenter', dur: '500'}}
								animation__mouseleave={{property: 'components.material.material.color', type: 'color', from: 'brown', to: 'pink', startEvents: 'mouseleave', dur: '500'}}
							>
							</Entity>
						)
					})
				}

			</a-scene>
	
		</div>
	);
}


const mapStateToProps = (state) => {
	return {
		xCoord: state.scene.xCoord,
		zCoord: state.scene.zCoord,
		sphereList: state.scene.sphereList,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onXCoordChanged: (x) => {
			dispatch(changeXCoord(x, 10))
		},

		onZCoordChanged: (z) => {
			dispatch(changeZCoord(z, 10))
		},
		onSphereAdded: (strX, strZ, floorWidth) => {

			let x = Number(strX)
			let z = Number(strZ)
			if (isNaN(x)) {
				alert("Invalid Input in X coordniate, should be a number!")
			} else if (isNaN(z)) {
				alert("Invalid Input in Z coordinate, should be a number!")
			} else if (x > floorWidth / 2 || x < - floorWidth / 2){
				alert("Coord X is out of floor boundary")
			} else if (z > floorWidth / 2 || z < - floorWidth / 2){
				alert("Coord Z is out of floor boundary")
			} else {
				dispatch(addSphere(x, z))
			}
		},

		onSphereDeleted: (sphereId) => {
			dispatch(deleteSphere(sphereId))
		},

	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SceneComponent)