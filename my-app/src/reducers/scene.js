const sceneReducer = (state = {
    sphereList: [],
    xCoord: null,
    zCoord: null
}, action) => {
    switch(action.type){
        case "ADD_SPHERE":
            return {
                ...state,
                sphereList: [ ...state.sphereList,
                    {
                        xCoord: action.xCoord,
                        zCoord: action.zCoord
                    }
                ],
                xCoord: null,
                zCoord: null,
            }
        
        case "DELETE_SPHERE":
            return {
                ...state,
                sphereList: state.sphereList.filter((sphere, index) => 
                    index !== action.sphereId
                )
            }

        case "CHANGE_XCOORD":
            return {
                ...state,
                xCoord: action.xCoord
            }
    
        case "CHANGE_ZCOORD":
            return {
                ...state,
                zCoord: action.zCoord
            }

        case "RESET_RESET_COORDS":
            return {
                ...state,
                xCoord: null,
                zCoord: null,
            }

        default:
            return state
    }
}

export default sceneReducer